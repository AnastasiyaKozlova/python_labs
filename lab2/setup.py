from setuptools import setup, find_packages

def read_file(name):
    with open(name) as file_name:
        return file_name.read()
setup(
    name='lab2',
    version='1.0',
    packages=find_packages(),
    author='anastasijaxx',
    description='This util can remove,restore files and work with trash',
    long_description=read_file("README.md"),
    entry_points={'console_scripts': 'smart_rm = smart_rm.main:main'},
    package_data={'smart_rm.config': ["default.json"]}
)
