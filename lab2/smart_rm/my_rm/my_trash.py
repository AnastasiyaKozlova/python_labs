#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module for usful methods for work with trash"""

import os
import logging
import pydoc

from urllib import unquote
from datetime import datetime
from smart_rm.config.policy import format_fileinfo
from smart_rm.my_rm.common_functions import TrashedFile


def find_appropriate_infofile(trash_file_path):
    """Find out name of file in trash_info_path.

    Args:
        - trash_file_path(str): file path in trash.

    Return:
        path of file .trashinfo.

    """
    trashfile_basename = os.path.basename(trash_file_path)
    string = []
    string.append(trashfile_basename)
    string.append('.trashinfo')
    trashinfo_basename = "{basename}.trashinfo".format(
        basename=trashfile_basename
    )
    file_dir = os.path.dirname(trash_file_path)
    trash_dir = os.path.dirname(file_dir)
    info_dir = os.path.join(trash_dir, 'info')
    return os.path.join(info_dir, trashinfo_basename)


def create_trash_dir(trash_path=None):
    """Create trash with files and info directories."""
    if trash_path is None:
        trash_path = os.path.join(os.path.realpath(os.curdir), "/my_trash")
    else:
        trash_path = os.path.join(os.path.realpath(os.curdir), trash_path)
    files_path = os.path.join(trash_path, "files")
    info_path = os.path.join(trash_path, "info")
    os.umask(0000)
    if not os.path.exists(trash_path):
        os.makedirs(trash_path, 0777)

    if not os.path.exists(files_path):
        os.makedirs(files_path, 0777)

    if not os.path.exists(info_path):
        os.makedirs(info_path, 0777)

    return trash_path


class Trash(object):
    """Used for showing files in trash and whole information about them.

    Args:
        - trash_path(str): trash path (default create trash in curr dir).
    """

    def __init__(self, trash_path="my_trash", max_file_size=100,
                 max_trash_size=100000, max_file_time=604800):
        self.logger = logging.getLogger('commands.my_trash')
        hdlr = logging.NullHandler()
        self.logger.addHandler(hdlr)
        if not os.path.exists(trash_path):
            self.trash_path = create_trash_dir(trash_path)
        else:
            self.trash_path = trash_path
        self.max_trash_size = max_trash_size
        self.max_file_size = max_file_size
        self.max_file_time = max_file_time
        self.files_path = os.path.join(self.trash_path, "files")
        self.info_path = os.path.join(self.trash_path, "info")

    def show(self):
        """Show files in trash in formatt output.

        For see trash in a good look import pydoc
        and call pydoc.pager(Trash().show())

        Return:
            page(string) with information about every file in trash.
        """

        trash_files_list = []
        if not os.path.exists(self.files_path):
            os.makedirs(self.files_path)
        if not os.path.exists(self.info_path):
            os.makedirs(self.info_path)
        trash_files = os.listdir(self.files_path)
        trash_files.sort()
        page = format_fileinfo("Name", "Type", "Path", "Deletion date",
                               "Size")
        page += "\n"
        for trash_file in trash_files:
            try:
                info_file = os.path.join(self.trash_path,
                                         find_appropriate_infofile(
                                         trash_file))
                trash_info = TrashInfoParser(open(info_file).read(),
                                                    info_file)
                name = unicode(trash_file)
                path = trash_info.get_original_location()
                deletion_date = trash_info.get_deletion_date()
                size = trash_info.get_size()
                type_ = trash_info.get_type()
                line = format_fileinfo(name, type_, path,
                                       deletion_date.strftime(
                                       "%Y-%m-%d %H:%M:%S"), size)
                page += line
                trashedfile = TrashedFile(name, path, deletion_date, info_file,
                                          os.path.join(self.trash_path,trash_file), size, type_,
                                          state="OK")
                trash_files_list.append(trashedfile)
            except Exception:
                self.logger.error("show: unknown file \'%s\'", trash_file)
                trash_files_list.append(TrashedFile(trash_file, file_path=os.path.join(self.trash_path,trash_file),
                                        state="unknown file"))
        #pydoc.pager(page)
        return trash_files_list

class TrashInfoParser(object):
    """Used for getting information from .trashinfo files in trash.

    Args:
        - contents(str): informaton about file.
        - volume_path(str): path to file .trashinfo.
    """

    def __init__(self, contents, volume_path):
        self.contents = contents
        self.volume_path = volume_path
        self.logger = logging.getLogger("commands.parser")
    def get_deletion_date(self):
        """Get file's deletion date from .trashinfo.

        Return:
            deletion date(datetime object) of file.
        """
        for line in self.contents.split('\n'):
            if line.startswith('DeletionDate='):
                try:
                    date = datetime.strptime(line[len('DeletionDate='):],
                                                            "%Y-%m-%d %H:%M:%S")
                    return date
                except ValueError:
                    self.logger.error("incorrect DeletionDate  \'%s\'",
                                        self.volume_path)

    def get_size(self):
        """Get file's size from .trashinfo.

        Return:
            size(float) of file.
        """

        for line in self.contents.split('\n'):
            if line.startswith('Size='):
                try:
                    size = line[len('Size='):]
                    return size
                except ValueError:
                    self.logger.error("incorrect Size  \'%s\'",
                                        self.volume_path)

    def get_original_location(self):
        """Get file's original location from .trashinfo

        Return:
            original location(str) of file.
        """

        for line in self.contents.split('\n'):
            if line.startswith('Path='):
                try:
                    path = unquote(line[len('Path='):])
                    return os.path.join(self.volume_path, path)
                except ValueError:
                    self.logger.error("Unable to parse Path \'%s\'",
                                       self.volume_path)

    def get_type(self):
        """Get file's type fron .trashinfo.

        Return:
            type(str) of file.
        """

        for line in self.contents.split('\n'):
            if line.startswith('Type='):
                try:
                    type_ = line[len('Type='):]
                    return type_
                except ValueError:
                    self.logger.error("incorrect Type  \'%s\'",
                                        self.volume_path)
