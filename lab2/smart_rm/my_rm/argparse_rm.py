"""Module argparse_rm is used to define command line parser."""

import argparse


def parse_args():
    """Parse arguments from console.

    Return:
        parser.
    """

    parser = argparse.ArgumentParser("smart_rm", conflict_handler='resolve',
									 description="this command removes/"+\
									 "restore your file/files + show"+\
									 " information about trash, my trash")
    group_mode = parser.add_mutually_exclusive_group()
    group_mode.add_argument("-v", "--verbose_mode",
                            help="explain what's being done",
					        action="store_true")
    group_mode.add_argument("-s", "--silent_mode", help="silent mode",
					        action="store_true")
    group_mode.add_argument("--dry_run", help="imitate command",
	 					    action="store_true")

    parser.add_argument("--config_file", type=str, default=None,
                        help="set config file")
    parser.add_argument("--trashdir", type=str, default=None,
                        help="set trash path")
    parser.add_argument("file", help="the file you wish to delete or restore",
                        type=str, nargs='*')
    parser.add_argument("-f", "--force_mode", help="ignore nonexistent"+\
                        " files and arguments", action="store_true")
    parser.add_argument("-i", "--interactive_mode", help="ask before"+\
                        " every removal", action="store_true")
    parser.add_argument("--regex", type=str, default=None,
                           help="remove by pattern")

    group_options = parser.add_mutually_exclusive_group()
    group_options.add_argument("-r", "--recursive_mode", help="remove"+\
                               " directories and it's contents recursivelyl",
                               action="store_true")
    group_options.add_argument("-d", "--empty_dir_mode",
                               help="remove empty directories",
						       action="store_true")
    group_options.add_argument("-rst", "--restore_mode", help="restore files"+\
                               " from trash", action="store_true")
    group_options.add_argument("-cl", "--clear_trash", help="remove files "+\
						       "from trash", action="store_true")
    group_options.add_argument("-s", "--show_trash_files", help="show fils "+\
                               "in my trash", action="store_true")
    return parser
