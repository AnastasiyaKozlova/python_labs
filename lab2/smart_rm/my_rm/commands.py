#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module Commands is created for calling commands in depending on args."""

import os
import logging
import smart_rm.config.policy as policy_module

from smart_rm.my_rm.my_trash import Trash
from smart_rm.my_rm.clear import ClearCommand
from smart_rm.config.settings import Settings
from smart_rm.my_rm.remove import RemoveCommand
from smart_rm.my_rm.common_functions import RemoveMethods


class Commands(object):
    """Check args and call appropriate functions for processing args.

    Args:
        - params(dictionary): could consist of user_trash_path or
                              user_config_file_path (default None).
    """

    def __init__(self, params=None):
        self.logger = initialize_logger()
        self.settings = self._get_settings(params)
        self.trash = self.settings.trash
        self.is_dry_run = False
        self.params = params


    def dry_run(self):
        """Turn on dry_run mode."""

        self.set_verbosity()
        self.is_dry_run = True

    def run(self, opts):
        """The start of checking args.

        Args:
            - opts(ArgumentParser object): args which user typed in
              console.
        """

        if opts['dry_run']:
            self.dry_run()

        files = opts.pop('file')
        regex = opts.pop('regex')

        ClearCommand(trash_path=os.path.expanduser(self.trash.path),
                     is_dry_run=self.is_dry_run, params=self.params
                     ).delete_by_size_date()

        if opts['silent_mode']:
            self.set_silence()
        elif opts['verbose_mode']:
            self.set_verbosity()

        if opts['force_mode']:
            self.settings.remove_policy.access_action = "force"
        elif opts['interactive_mode']:
            self.settings.remove_policy.access_action = "ask"

        if opts['restore_mode']:
            self.restore(files, regex)
        elif opts['show_trash_files']:
            self.show()
        elif opts['clear_trash']:
            self.clear(files, regex)
        elif opts['recursive_mode']:
            remove_method = RemoveMethods.RECURSIVE_MODE
            self.move_to_trash(remove_method, files, regex)
        elif opts['empty_dir_mode']:
            remove_method = RemoveMethods.EMPTY_DIR_MODE
            self.move_to_trash(remove_method, files, regex)
        else:
            remove_method = RemoveMethods.FILE_MODE
            self.move_to_trash(remove_method, files, regex)

        #return self.reporter.exit_code()

    def _get_settings(self, params):
        try:
            return Settings(params)
        except Exception as ex:
            self.logger.error("%s", str(ex))
            exit()

    def set_verbosity(self):
        """Set verbosity mode"""

        self.logger.setLevel(logging.INFO)

    def set_silence(self):
        """Set silence mode."""

        self.settings.remove_policy.access_action = "force"
        self.logger.setLevel(logging.FATAL)


    def move_to_trash(self, remove_method, files, regex):
        """Calls RemoveCommand().run() to remove files.

        Args:
            - remove_method(str): define policy for remove.
            - files(list): list of files to remove.
            - regex(str): define pattern for removed files.
         """

        policy = self._select_remove_policy()
        RemoveCommand(trash_path=os.path.expanduser(self.trash.path),
                      policy=policy,
                      is_dry_run=self.is_dry_run, remove_method=remove_method,
                      params=self.params, regex=regex).run(files)

    def restore(self, files, regex):
        """Calls RestoreCommand().run() to restore files.

        Args:
            - files(list): list of files to restore.
        """

        from .restore import RestoreCommand
        RestoreCommand(params=self.params, trash_path=os.path.expanduser(
                       self.trash.path), is_dry_run=self.is_dry_run,
                       regex=regex).run(files)

    def clear(self, files, regex):
        """Calls ClearCommand().run() to remove files from trash.

        Args:
            - files(list): list of files to remove from trash.
        """

        ClearCommand(trash_path=os.path.expanduser(self.trash.path),
                     is_dry_run=self.is_dry_run, regex=regex,
                     params=self.params).run(files)
    def show(self):
        """Calls Trash().show() to show trash content."""

        Trash(trash_path=os.path.expanduser(self.trash.path)).show()

    def _select_remove_policy(self):
        if self.settings.remove_policy.access_action == "force":
            return policy_module.ForcePolicy()
        elif self.settings.remove_policy.access_action == "ask":
            return policy_module.AskPolicy()
        elif self.settings.remove_policy.access_action == "default":
            return policy_module.DefaultPolicy()

def initialize_logger():
    """Initialize logger.

    Return:
        logger.
    """

    logger = logging.getLogger('commands')
    logger.setLevel(logging.WARNING)
    stream = logging.StreamHandler()
    stream.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    stream.setFormatter(formatter)
    logger.addHandler(stream)
    return logger
