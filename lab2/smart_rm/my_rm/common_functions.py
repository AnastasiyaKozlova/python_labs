import os
import re

class RemoveMethods(object):
    """Constant value for remove_policy."""

    RECURSIVE_MODE = "remove_recursive"
    EMPTY_DIR_MODE = "remove_empty_dir"
    FILE_MODE = "remove_file"


class TrashedFile(object):
    """Represent a trashed file.

    Args:
        - original_location(str): the original path from where the file has been
                                  trashed.
        - deletion_date(datetime object): the time when the file has been
                                          trashed.
        - info_file(str): the file that contains information.
        - file_path(str): the path where the trashed file has been placed after
                          the trash opeartion.
        - size(str): the size of file.
        - type(str): the type of file (file, link, directory).
        - state(str): the condition of process ("OK" or string with error
                          message)
    """

    def __init__(self, name=None, original_location=None, deletion_date=None,
                 info_file=None, file_path=None, size=None, type_=None,
                 state=None, error_message=''):
        self.name = name
        self.original_location = original_location
        self.deletion_date = deletion_date
        self.info_file = info_file
        self.file_path = file_path
        self.size = size
        self.type = type_
        self.state = state
        self.error_message = error_message


def get_files_by_regex(regex, files):
    """Collects files by pattern.

    Args:
        - regex(str): pattern to choose file in directory.
        - files(list): list of files in directory.

    Return:
        appropriate file list to pattern.
    """
    def walk(arg, dirname, files):
        """Required for os.path.walk, pick up files with pattern.

        Args:
            - arg(no matter): for nothing, but required function.
            - dirname(str): dirname to find files.
            - files(list): files to choose appropriate ones.
        """

        for file_name in files:
            if re.search(regex, file_name):
                file_path = os.path.join(dirname, file_name)
                file_list.append(file_path)

    files_list = []
    for file_path in files:
        file_list = []
        os.path.walk(file_path, walk, file_list)
        files_list.extend(file_list)
    return files_list
