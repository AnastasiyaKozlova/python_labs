#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module clear is used for clear trash."""

import os
import shutil
import logging
import smart_rm.config.policy as policy

from smart_rm.config.settings import Settings
from smart_rm.my_rm.my_trash import TrashInfoParser
from smart_rm.my_rm.my_trash import create_trash_dir
from smart_rm.my_rm.my_trash import find_appropriate_infofile
from smart_rm.my_rm.common_functions import get_files_by_regex
from smart_rm.config.settings import get_timedelta
from smart_rm.config.settings import get_size
from smart_rm.my_rm.common_functions import TrashedFile



class ClearCommand(object):
    """Class Clear is created to work with trash, clear it.

    Args:
        - trash_path(str): trash path (default create trash in curr dir).
        - is_dry_run(bool): flag for imitation work (default False).
        - params(dictionary): could consist of user_trash_path or
                              user_config_file_path (default None).
        - regex(str): pattern for deletion files (default None).
    """

    def __init__(self, trash_path="my_trash",
                 is_dry_run=False, params=None, regex=None):
        self.is_dry_run = is_dry_run
        self.settings = self._get_settings(params)
        if not os.path.exists(trash_path):
            self.path = create_trash_dir(trash_path)
        else:
            self.path = trash_path
        self.files_path = os.path.join(self.path, 'files')
        self.info_path = os.path.join(self.path, 'info')
        self.logger = logging.getLogger('commands.clear')
        hdlr = logging.NullHandler()
        self.logger.addHandler(hdlr)
        self.clear_policies = None
        self.regex = regex

    def run(self, files=None):
        """Take files and try to delete it and its .trashinfo.

        Args:
            - files(list): list of files to be deleted from trash.
        """
        clear_files_list = []
        files_list = []
        trash_list = list_files_in_dir(self.files_path)
        if self.regex:
            files_list = get_files_by_regex(self.regex, files)
        elif not files or files == [""]:
            files_list = trash_list
        else:
            files_list.extend(files)
        for trash_file in files_list:
            try:
                trashfile = os.path.basename(trash_file)
                file_path = os.path.join(self.files_path, trashfile)
                if file_path in trash_list:
                    try:
                        info_file = find_appropriate_infofile(file_path)
                        trash_info = TrashInfoParser(open(info_file).read(),
                                                                    info_file)
                        original_location = trash_info.get_original_location()
                        deletion_date = trash_info.get_deletion_date()
                        trashedfile = TrashedFile(original_location,
                                                  deletion_date,
                                                  info_file, file_path,
                                                  trash_info.get_size,
                                                  trash_info.get_type,
                                                  state="OK")
                        self.delete_trashinfo_and_file(info_file)
                        clear_files_list.append(trashedfile)
                    except Exception as ex:
                        self.logger.error("clear: can not clear \'%s\':%s",
                                            file_path, str(ex))
                        clear_files_list.append(TrashedFile(file_path=file_path,
                                                state="can not clear"))
                else:
                    raise ValueError("file does not exist")
            except Exception as ex:
                self.logger.error("clear: file does not exist in trash :%s",
                                        file_path)
                clear_files_list.append(TrashedFile(file_path=file_path,
                                        state=str(ex)))
        return clear_files_list

    def delete_trashinfo_and_file(self, trashinfo_path):
        """Delete .trashinfo and file.

        Args:
            - trashinfo_path(str): path to file .trashinfo.
        """

        self.logger.info("attempt to total_remove \'%s\'", trashinfo_path)
        trashcan = CleanableTrashcan(self.is_dry_run, self.logger)
        trashcan.delete_trashinfo_and_backup(trashinfo_path)
        self.logger.info("cleared \'%s\'", trashinfo_path)

    def delete_by_size_date(self, value_date=None, value_size=None):
        """Delete automatic files in trash.

        Args:
            - value_date(string): defines max_time for keeping("off",
              "N.y/m/d/H/M").
            - value_size(string): defines max_size for keeping("off",
              "N.b/kb/mb").
        """

        self.logger.info("automatic cleaning is started")
        if self.clear_policies is None:
            self.clear_policies = self._select_clear_policies(value_date,
                                                              value_size)
        if not os.path.exists(self.files_path):
            os.makedirs(self.files_path)
        if not os.path.exists(self.info_path):
            os.makedirs(self.info_path)
        info_files = list_files_in_dir(self.info_path)
        files_for_remove = []
        for info_file in info_files:
            try:
                trash_info = TrashInfoParser(open(info_file).read(),
                                                        info_file)
                files_for_remove.append((info_file, trash_info))
            except Exception as ex:
                self.logger.info("trash/info:  \'%s\'", str(ex))
        cleanable_trash = set()
        for policy in self.clear_policies:
            cleanable_trash.update(policy.get_trash(files_for_remove))
        try:
            for trash_file in cleanable_trash:
                self.delete_trashinfo_and_file(trash_file)
        except Exception as ex:
            self.logger.error("clear: file \'%s\' was not cleared\nerror: %s",
                              trash_file, str(ex))
        self.logger.info("automatic cleaning is finished")

    def _select_clear_policies(self, value_date=None, value_size=None):
        policies = []
        if value_date is None:
            value_date = self.settings.clear_policy.by_date
        else:
            value_date = get_timedelta(value_date)
        if value_size is None:
            value_size = self.settings.clear_policy.by_size
        else:
            value_size = get_size(value_size)
        if not value_date == "off":
            policies.append(policy.ClearByDatePolicy(value_date))
        if not value_size == "off":
            policies.append(policy.ClearBySizePolicy(value_size))
        return policies

    def _get_settings(self, params):
        try:
            return Settings(params)
        except Exception as ex:
            self.logger.error("%s", str(ex))
            exit()

class CleanableTrashcan(policy.DryRun, object):
    """Delete files from trash.

    Args:
        - is_dry_run(bool): flag for imitation work.
        - logger(logging): show message of error.
    """

    def __init__(self, is_dry_run, logger):
        self.logger = logger
        self.is_dry_run = is_dry_run

    def _delete_orphan(self, path_to_backup_copy):
        self.logger.info("delete_orphan")
        try:
            return remove_file(path_to_backup_copy)
        except OSError as ex:
            self.logger.error("cannot defined path to back up copy: \'%s\'",
                              path_to_backup_copy)

    def delete_trashinfo_and_backup(self, trashinfo_path):
        """Delete file and .trashinfo from trash.

        Args:
            - trashinfo_path(str): path to file .trashinfo.
        """

        self.logger.info("delete_trashinfo_and_backup")
        backup_copy = path_of_backup(trashinfo_path)
        self.dry_run(self._delete_orphan)(backup_copy)
        self.dry_run(self._delete_orphan)(trashinfo_path)

def path_of_backup(path_to_trashinfo):
    """Find path of file by .trashinfo.

    Args:
        - path_to_trashinfo(str): path file .trashinfo.

    Return:
        path to removed file.
    """

    trash_dir = os.path.dirname(os.path.dirname(path_to_trashinfo))
    return os.path.join(trash_dir, 'files',
                os.path.basename(path_to_trashinfo)[:-len('.trashinfo')])


def list_files_in_dir(path):
    """Collect file in directory.

    Args:
        - path(str): path to dir.

    Return:
        list of all files in directory.
    """

    result = []
    for entry in os.listdir(path):
        result.append(os.path.join(path, entry))
    return result

def remove_file(path):
    """Just remove file.

    Args:
        - path(str): file path to remove.

    Raise:
        TypeError: if file has undefined type.
        ValueError: if file does not exists.
    """

    if os.path.exists(path):
        try:
            if os.path.isfile(path):
                os.remove(path)  # remove the file
            elif os.path.isdir(path):
                shutil.rmtree(path)  # remove dir and all contains
        except:
            raise TypeError("file {} is not a file or dir.".format(path))
    else:
        raise ValueError("file does not exist")
