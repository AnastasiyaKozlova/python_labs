import os
import shutil
import unittest
import pdb

from smart_rm.my_rm.file_methods import get_fileinfo
from smart_rm.config.policy import DefaultPolicy, ForcePolicy
from smart_rm.my_rm.remove import RemoveCommand
from smart_rm.my_rm.restore import RestoreCommand

class TestRestoreMethods(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.trash_path = os.path.join(self.dirpath, "Trash")
        if not os.path.exists("root/dir1"):
            os.makedirs("root/dir1")
        if not os.path.exists("root/dir2"):
            os.makedirs("root/dir2")
        with open("root/file", 'w') as file_name:
            file_name.write("qwertyui")
        with open("root/dir1/file1", 'w') as file_name:
            file_name.write("qwer")
        with open("root/dir1/file2", 'w') as file_name:
            file_name.write("q")
        self.dir_one = get_fileinfo("root/dir1")
        self.dir_two = get_fileinfo("root/dir2")
        self.file_one = get_fileinfo("root/dir1/file1")
        self.policy = DefaultPolicy()
        RemoveCommand(remove_method="remove",
                      policy=ForcePolicy(),
                      trash_path=self.trash_path
                      ).put_to_trash(self.file_one)
        RemoveCommand(remove_method="remove",
                      policy=ForcePolicy(),
                      trash_path=self.trash_path
                      ).put_to_trash(self.dir_two)

    def tearDown(self):
        if os.path.exists("root"):
            shutil.rmtree("root")
        if os.path.exists(self.trash_path):
            shutil.rmtree(self.trash_path)

    def test_restore(self):

        file_path = os.path.join(self.trash_path, "files", self.file_one.name)
        name = self.file_one.name
        info_name = name + ".trashinfo"
        info_path = os.path.join(self.trash_path, "info", info_name)
        RestoreCommand(trash_path=self.trash_path
                       ).restore(file_path, self.policy, self.policy)

        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.file_one.original_location))

        file_path = os.path.join(self.trash_path, "files", self.dir_two.name)
        name = self.dir_two.name
        info_name = name + ".trashinfo"
        info_path = os.path.join(self.trash_path, "info", info_name)
        RestoreCommand(trash_path=self.trash_path
                       ).restore(file_path, self.policy, self.policy)
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.dir_two.original_location))

    def test_dry_run_restore(self):
        file_path = os.path.join(self.trash_path, "files", self.file_one.name)
        name = self.file_one.name
        info_name = name + ".trashinfo"
        info_path = os.path.join(self.trash_path, "info", info_name)
        RestoreCommand(trash_path=self.trash_path, is_dry_run=True
                       ).restore(name, self.policy, self.policy)
        self.assertTrue(os.path.exists(file_path))
        self.assertTrue(os.path.exists(info_path))
        self.assertFalse(os.path.exists(self.file_one.original_location))

    def test_restore_with_uniq_name(self):
        open("root/dir1/file1", 'w').close()
        file_path = get_fileinfo("root/dir1/file1")
        RemoveCommand(remove_method="remove",
                      policy=ForcePolicy(),
                      trash_path=self.trash_path
                      ).put_to_trash(file_path)
        name = file_path.name
        file_trash_path = os.path.join(self.trash_path, "files", name)
        RestoreCommand(trash_path=self.trash_path
                       ).restore(file_trash_path, self.policy, self.policy)
        #pdb.set_trace()
        file_trash_path_one = os.path.join(self.trash_path, "files",
                                           name + ".1")
        RestoreCommand(trash_path=self.trash_path
                       ).restore(file_trash_path_one, self.policy, self.policy)
        self.assertTrue(os.path.exists(file_path.original_location))
        self.assertTrue(os.path.exists(file_path.original_location + ".1"))

    def test_restore_in_non_exist_folder(self):
        shutil.rmtree(self.dir_one.original_location)
        self.assertFalse(os.path.exists(self.dir_one.original_location))
        file_path = os.path.join(self.trash_path, "files", self.file_one.name)
        name = self.file_one.name
        info_name = name + ".trashinfo"
        info_path = os.path.join(self.trash_path, "info", info_name)
        RestoreCommand(trash_path=self.trash_path
                       ).restore(file_path, self.policy, self.policy)
        self.assertFalse(os.path.exists(file_path))
        self.assertFalse(os.path.exists(info_path))
        self.assertTrue(os.path.exists(self.file_one.original_location))


if __name__ == "__main__":
    unittest.main()
