"""Module confids is used for getting config options from default file and
user one.
"""

import os
import json
from ConfigParser import ConfigParser


class Config(object):
    """Class Config is created to get config options settings"""

    CONFIG_PATH = "~/.local/share/.trashconfig"

    def __init__(self):
        self.config = ConfigParser()

    def get_default_config(self):
        """Method get_default_config is used to take options from default config
         file.
         """

        dirname = os.path.dirname(os.path.realpath(__file__))
        config_path = os.path.join(dirname, "default.json")
        content = self.read_json(config_path)
        return content

    def get_user_config(self, config_path=CONFIG_PATH):
        """Take options from user config file.

        Args:
            - config_path(str): path to user_config_file.

        Raises:
            Exception: if incorrect config file.

        Return:
            config(dictionary): {config_name: value}.
         """

        config_path = os.path.expanduser(config_path)
        config = {}
        try:
            config = self.read_config(config_path)
        except Exception:
            raise Exception("trash: incorrect config file")
        return config

    def read_config(self, config_path):
        """Read data from user file.

        Args:
            - config_path(str): path to user_config_file.

        Return:
            config(dictionary): {config_name: value}.
        """

        config = {}
        self.config.read(config_path)

        for section in self.config.sections():
            config[section] = {}
            for option in self.config.options(section):
                config[section][option] = self.config.get(section, option)
        return config

    def read_json(self, config_path):
        """Read comfigs from default_config_file.

        Args:
            - config_path(str): path to user_config_file.

        Raises:
            Exception: if incorrect config json file.

        Return:
            config(dictionary): {config_name: value}.
         """

        config = {}
        with open(config_path) as configfile:
            try:
                config = json.load(configfile)
            except Exception:
                raise Exception("trash: incorrect config json file")
        return config

    def merge(self, configs_array):
        """Merge default configs with user additionals.
        Args:
            - config_array(list): defaulte configs and user ones.

        Return:
            config(dictionary): {config_name: value}.
         """

        result = {}
        for config in configs_array:
            for section, options in config.iteritems():
                if section not in result:
                    result[section] = {}
                content = {k: v for k, v in options.iteritems() if v != None}
                result[section].update(content)
        return result
