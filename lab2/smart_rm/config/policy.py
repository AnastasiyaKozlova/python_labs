#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module policy is used to define policy for program commands."""

import os
from datetime import datetime

class Policy(object):
    """Class Policy is mixin for AskPolicy, DefaultPolicy and
    ForcePolicy.
    """

    def step_to_dir(self, directory, iteration_func):
        """Define how to go down to the dir.

        Args:
            - directory(str): directory path wanted to go down.
            - iteration_func(str): name of function that is used to make step to
                                   dir.

        Return:
            If it's available directory, iteration_func is returned.
            Else - calling AskPolicy().step_to_dir().
        """

        if not os.access(directory.original_location, os.W_OK):
            return AskPolicy().step_to_dir(directory, iteration_func)
        else:
            return iteration_func()

    def confirm_action(self, prompt, func, *args):
        """Define to ask user or not for doing some actions.

        Args:
            - prompt(str): message for user in interactive mode.
            - func(function): some function, which action is need to be
                              confirmed.
            - *args(no matter): argument of func.

        Return:
            True if user confirms action, False if not.
        """

        response = False
        if request(prompt):
            func(*args)
            response = True
        return response

    def remove(self, item, trash_path):
        """Remove file.

        Args:
            - item(file oject): file, that is wanted to be remove.
            - trash_path(str): path to trash.

        Return:
            True if there is permition access for remove and it's OK.
            Else - it depends on user answer to delete.
        """

        if not os.access(item.original_location, os.W_OK):
            prompt = "rm: remove write-protected {0} \'{1}\'? ".\
                format(item.type, item.original_location)
            status = self.confirm_action(prompt, item.move, trash_path)
        else:
            item.move(trash_path)
            status = True
        return status


class AskPolicy(Policy,object):
    """Class AskPolicy is created for interactive mode."""

    def restore(self, file_path, dest_path):
        """Defines how to restore file from trash.

        Args:
            - files_path(str): path move from.
            - dest_path(str): path move to.

        Return:
            True if there is permition access for remove and it's OK.
            Else - it depends on user answer to delete.
        """

        status = False
        prompt = "restore: path, inclusive this file, is not exists. Create? "
        dirname = os.path.dirname(dest_path)
        if not os.path.exists(dirname):
            status = self.confirm_action(prompt, os.renames,
                                         file_path, dest_path)
        return status

    def get_destination(self, path):
        """Define how to deal with similar files in destination.

        Args:
            - path(str): path move to.

        Reise:
            Exception: if file exists and user doesn't want to replace it.

        Return:
            path(str) move to.
        """

        prompt = "restore: file name already exists in folder\n" + \
                 "Replace file? "
        if request(prompt):
            return path
        else:
            raise Exception("this name is already taken")

    def remove(self, item, trash_path):
        prompt = "rm: remove  \'{0}\'? ".\
            format(item.original_location)
        if request(prompt):
            item.move(trash_path)
            return True
        else:
            return False

    def step_to_dir(self, directory, iteration_func):
        prompt = "rm: step to the  \'{0}\'? ".\
            format(directory.original_location)
        if request(prompt):
            return iteration_func()
        else:
            return 0

class DefaultPolicy(Policy, object):
    """Class DefaultPolicy is created to define politicy of base operations
    without additionals.
    """

    def get_destination(self, trash_info_basename):
        """Creates uniq  name if it's required.

        Args:
            - path(str): path move to.

        Return:
            path(str) move to.
        """

        preffix, ext = os.path.splitext(trash_info_basename)
        index = 0
        while os.path.exists(trash_info_basename):
            index += 1
            if index == 0:
                suffix = ""
            else:
                preffix_and_suffix, _ = os.path.splitext(trash_info_basename)
                preffix, _ = os.path.splitext(preffix_and_suffix)
                suffix = ".%d" % index
            trash_info_basename = preffix+suffix+ext
        return trash_info_basename

    def restore(self, file_path, dest_path):
        """Defines how to restore file from trash.

        Args:
            - files_path(str): path move from.
            - dest_path(str): path move to.

        Return:
            True in anyway.
        """

        os.renames(file_path, dest_path)
        return True


class ForcePolicy(Policy, object):
    """Class ForcePolicy is used for rude remove without any ask."""

    def remove(self, item, trash_path):
        item.move(trash_path)
        return True

    def step_to_dir(self, directory, iteration_func):
        return iteration_func()


class DryRun(object):
    """Class DryRun is mixin for RemoveCommand and ClearCommand. Used to show
    what will happen if do smth."""

    def dry_run(self, func):
        """Return:
            result of wrapped_func.
        """

        def wrapped_func(*args):
            """Called to make imitation of work.

            Return:
                True: if self.is_dry_run is true.
                Else - call func with args.
            """

            if self.is_dry_run:
                return True
            else:
                return func(*args)
        return wrapped_func

def request(question, force=False):
    """Ask user what to do.

    Function request depands on mode. If program works in ForcePolicy,
    user is not asked about permition. In another way program asks user
    what to do, if answer is correct program will work correctly. In another way
    it will reask.

    Args:
        - question(str): what to ask.
        - force(bool): force_mode flag.

    Return:
        True: user confirms action.
        False: user rejects.
    """

    if force:
        return True
    positive = ['y', 'yes']
    answer = raw_input(question)
    if answer.lower() in positive:
        return True
    else:
        return False

class ClearByDatePolicy(Policy, object):
    """Used in automaticly clear. Defines timedelta.

    Args:
        - timedelta(str): max time to keep files in trash.
    """

    def __init__(self, timedelta):
        self.timedelta = timedelta

    def get_trash(self, files):
        """Collects overdue files.

        Args:
            - files(list): list of files from trash.

        Return:
            list of overdue files.
        """

        result = set()
        now = datetime.now()
        for file_name, info in files:
            deletion_date = info.get_deletion_date
            if deletion_date is not None:
                if now - deletion_date() > self.timedelta:
                    result.add(file_name)
        return result


class ClearBySizePolicy(Policy, object):
    """Used in automaticly clear. Defines overweight files.

    Args:
        - max_size(str): max_size to keep files in trash.
    """

    def __init__(self, max_size):
        self.max_size = max_size

    def get_trash(self, files):
        """Collects overweight files.

        Args:
            - files(list): list of files from trash.

        Return:
            list of overweight files.
        """

        result = set()
        for file_name, size in files:
            int_size = int(size.get_size())
            if int_size > self.max_size:
                result.add(file_name)
        return result

def format_size(size):
    """Define what extention write to size number.

    Args:
        - size(float): size of file.

    Return:
        formatt string of size with extention.
    """

    size = int(size)
    BYTE_PER_KB = 1024
    BYTE_PER_MB = 1024 * BYTE_PER_KB
    BYTE_PER_GB = 1024 * BYTE_PER_MB
    if size < BYTE_PER_KB:
        return str(size) + "b"
    elif size < BYTE_PER_MB:
        return str(size / BYTE_PER_KB) + "kb"
    elif size < BYTE_PER_GB:
        return str(size / BYTE_PER_MB) + "mb"
    else:
        return str(size / BYTE_PER_GB) + "gb"


def format_fileinfo(name, ftype, path, deletion_date, size):
    """Print trashinfo in appropriate way.

    (Numbers - 10, 20, 55 - is chosen for printing info by Empirical method)

    Args:
        - name(str): name of file.
        - ftype(str): file type.
        - path(str): original file path.
        - deletion_date(str): file deletion date.
        - size(str): file size.

    Return:
        formatt string with file information.
    """
    return "%*s " % (-10, name) +  "%*s " % (-55, path.decode('utf-8')) +\
           "{0:10} {1:20} {2:10}\n".format(ftype, deletion_date, size)
