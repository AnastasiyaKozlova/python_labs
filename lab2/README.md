# smart_rm bash utility #

Smart_rm package is improved version of trash bin.

### RM mode ###

Move files to trash bin:
```
$ smart_rm filename1 filename2
```

Move empty directory to trash bin:
```
$ smart_rm -d dirname
$ smart_rm --empty_dir_mode dirname
```

Move not empty directory to trash bin:
```
$ smart_rm -r dirname
$ smart_rm --recursive_mode dirname
```

Move file by regex:
```
$ smartrm --regex "regex_expression" dirname
```

Move file in dry_run mode:
```
$ smartrm --dry_run filename
```

Remove file to custom trash:
```
$ smart_rm --trashdir path_to_user_trash filename
```

### TRASH mode ###

Show files that already in trash:
```
$ smart_rm -s
$ smartrash --show_trash_files
```

Clear trash from files:
```
$ smart_rm -cl filename
$ smart_rm --clear_trash filename
```

Clear trash from whole files:
```
$ smart_rm -cl
$ smart_rm --clear_trash
```

### RESTORE mode ###

Restore files from trash bin:
```
$ smart_rm -rst filename1 filename2
$ smart_rm --restore_mode filename1 filename2
```


### Other program flags ###

#### Force-Mode ####
To run program without any promts or warning messages
Use flag ```--force_mode``` or ```-f```

#### Interactive-Mode ####
To run program with promts
Use flag ```--interactive_mode``` or ```-i```

#### Silent-Mode ####
To run program without any promts and printing current processes
Use flag ```--silent``` or ```-s```

#### Verbose-Mode ####
To run program with detail information what is done
Use flag ```--verbose_mode``` or ```-v```



### Run with applying custom policies ###

To run program with your policies use flags ```--config-file``` whare you can
define such politics as trash_path, max_size of trash,
politic of name_duplicate, clear_policy by date and size.
```
$ smart_rm --config_file filename
```

### SetUp instructions ###

$ sudo python setup.py install
```
